<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

use CRM_Opencaserearranger_ExtensionUtil as E;

/**
 * Form controller class
 *
 * @see https://wiki.civicrm.org/confluence/display/CRMDOC/QuickForm+Reference
 */
class CRM_Opencaserearranger_Form_Settings extends CRM_Core_Form {

  public function buildQuickForm() {

    // add form elements
    $options = $this->getFieldOptions();
    for ($i = 1; $i <= count($options); $i++) {
      $this->add(
        'select', // field type
        "casefield_{$i}", // field name
        E::ts('Position %1', array(1 => $i)), // field label
        $options, // list of options
        TRUE // is required
      );
    }
    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => E::ts('Save'),
        'isDefault' => TRUE,
      ),
    ));

    // export form elements
    $this->assign('elementNames', $this->getRenderableElementNames());

    $this->addFormRule(array($this, 'formRule'));

    parent::buildQuickForm();
  }

  public function setDefaultValues() {
    $savedSettings = \Civi::settings()->get('open_case_field_order');
    $defaults = array();

    if (empty($savedSettings)) {
      // No previously saved settings, so set the default to be the same order as getFieldOptions returns.
      // Note this will be indexed starting from 0, hence the +1.
      foreach (array_keys($this->getFieldOptions()) as $i => $key) {
        $defaults["casefield_" . ($i+1)] = $key;
      }
    } else {
      // These are already indexed starting from 1.
      foreach ($savedSettings as $i => $key) {
        $defaults["casefield_{$i}"] = $key;
      }
    }
    return $defaults;
  }

  public function formRule($fields, $files, $self) {
    $alreadySelected = array();
    $errors = array();
    foreach ($fields as $k => $v) {
      if (isset($alreadySelected[$v])) {
        $errors[$k] = E::ts("Can't select the same field twice.");
        break;
      } else {
        $alreadySelected[$v] = 1;
      }
    }
    if (empty($errors)) {
      return TRUE;
    }
    return $errors;
  }

  public function postProcess() {
    $values = $this->exportValues();
    $options = $this->getFieldOptions();

    $valsToSave = array();
    for ($i = 1; $i <= count($options); $i++) {
      $valsToSave[$i] = $values["casefield_{$i}"];
    }
    \Civi::settings()->set('open_case_field_order', $valsToSave);

    CRM_Core_Session::setStatus(E::ts('Settings saved.'), '', 'success', array('expires' => 0));
    parent::postProcess();
  }

  public function getFieldOptions() {
    $options = array(
      'tr.crm-case-form-block-medium_id' => E::ts('Medium'),
      'tr.crm-case-form-block-activity_details' => E::ts('Details'),
      // This one is special. It doesn't have any identifier, so our javascript
      // attempts to locate it and add this class.
      'tr.opencaserearranger-custom' => E::ts('Activity Custom Fields'),
      'tr.crm-case-form-block-activity_subject' => E::ts('Subject'),
      'tr.crm-case-opencase-form-block-case_type_id' => E::ts('Case Type'),
      'tr.crm-case-opencase-form-block-status_id' => E::ts('Case Status'),
      'tr.crm-case-opencase-form-block-start_date' => E::ts('Start Date'),
      'tr.crm-activity-form-block-attachment' => E::ts('Attachments'),
      'tr.crm-case-form-block-duration' => E::ts('Duration'),
      'tr.crm-case-form-block-tag' => E::ts('Tags'),
      'tr.crm-case-form-block-custom_data' => E::ts('Case Custom Fields'),
      'tr.crm-case-form-block-tag_set' => E::ts('Tagsets'),
    );
    return $options;
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  public function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }

}
