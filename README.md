# Open Case Form Fields Rearranger

If you are using CiviCase and don't like the order of the fields when creating a New Case, install this extension and re-order them.

The extension is licensed under [MIT](LICENSE.txt).

## Requirements

* PHP v7+
* CiviCRM (5.x)

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl https://lab.civicrm.org/extensions/opencaserearranger/-/archive/master/opencaserearranger-master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/opencaserearranger.git
cv en opencaserearranger
```

## Usage

After installation go to Administer -> CiviCase -> Open Case Order.

## Known Issues

This extension may not play nice if you have already customized the Open Case screen, but if you've done that then you don't need this extension.

TODO: The config screen might be nicer as drag and drop.
