<?php
/* vim: set softtabstop=2 tabstop=2 shiftwidth=2: */

require_once 'opencaserearranger.civix.php';
use CRM_Opencaserearranger_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function opencaserearranger_civicrm_config(&$config) {
  _opencaserearranger_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function opencaserearranger_civicrm_xmlMenu(&$files) {
  _opencaserearranger_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function opencaserearranger_civicrm_install() {
  _opencaserearranger_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function opencaserearranger_civicrm_postInstall() {
  _opencaserearranger_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function opencaserearranger_civicrm_uninstall() {
  _opencaserearranger_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function opencaserearranger_civicrm_enable() {
  _opencaserearranger_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function opencaserearranger_civicrm_disable() {
  _opencaserearranger_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function opencaserearranger_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _opencaserearranger_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function opencaserearranger_civicrm_managed(&$entities) {
  _opencaserearranger_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function opencaserearranger_civicrm_caseTypes(&$caseTypes) {
  _opencaserearranger_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function opencaserearranger_civicrm_angularModules(&$angularModules) {
  _opencaserearranger_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function opencaserearranger_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _opencaserearranger_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_entityTypes
 */
function opencaserearranger_civicrm_entityTypes(&$entityTypes) {
  _opencaserearranger_civix_civicrm_entityTypes($entityTypes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function opencaserearranger_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
 */
function opencaserearranger_civicrm_navigationMenu(&$menu) {
  _opencaserearranger_civix_insert_navigation_menu($menu, 'Administer/CiviCase', array(
    'label' => E::ts('Open Case Order'),
    'name' => 'open_case_order',
    'url' => 'civicrm/admin/case/opencaserearranger?reset=1',
    'permission' => 'administer CiviCase',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _opencaserearranger_civix_navigationMenu($menu);
}

function opencaserearranger_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Case_Form_Case' && $form->_action == CRM_Core_Action::ADD) {
    $savedSettings = \Civi::settings()->get('open_case_field_order');
    if (empty($savedSettings)) {
      return;
    }
    $settingsForm = new CRM_Opencaserearranger_Form_Settings();
    $optionsCount = count($settingsForm->getFieldOptions());
    $templateVars = array(
      'orderedFields' => array(
      ),
    );
    for ($i = 1; $i <= $optionsCount; $i++) {
      // Need to handle $i==1 specially.
      $templateVars['orderedFields'][] = array(
        'toMove' => $savedSettings[$i],
        'after' => (
          ($i == 1)
          ? 'div.crm-case-form-block table tr:first'
          : $savedSettings[$i - 1]
        ),
      );
    }

    $js = $form->getTemplate()->fetchWith('CRM/Case/Form/caseRearranged.js.tpl', $templateVars);
    CRM_Core_Resources::singleton()->addScript($js);
  }
}
