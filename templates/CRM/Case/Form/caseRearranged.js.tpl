{literal}
(function($) {
  $(document).ready(function() {
    // If activity custom fields are present, we need to mark them with a
    // class since they have no class. With the default ordering, they appear
    // after Details.
    var maybeActivityCustomFields = $('tr.crm-case-form-block-activity_details').next('tr');
    if ((typeof maybeActivityCustomFields.attr('class')) == 'undefined') {
      maybeActivityCustomFields.addClass('opencaserearranger-custom');
    }
    {/literal}{foreach from=$orderedFields item=fieldSpec}{literal}
      if ($('{/literal}{$fieldSpec.toMove}{literal}').length && $('{/literal}{$fieldSpec.after}{literal}').length) {
        $('{/literal}{$fieldSpec.toMove}{literal}').insertAfter('{/literal}{$fieldSpec.after}{literal}');
      }
    {/literal}{/foreach}{literal}
  });
})(CRM.$);
{/literal}
